package au.com.dius;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class MatchTest {
    @Test
    public void matchInProgress() {
        Player p1 = new Player("p1");
        Player p2 = new Player("p2");
        Set set = mock(Set.class);
        when(set.score()).thenReturn("1-0");
        SetFactory setFactory = mock(SetFactory.class);
        when(setFactory.create(p1, p2)).thenReturn(set);
        PlayerFactory playerFactory = mock(PlayerFactory.class);
        when(playerFactory.create("p1")).thenReturn(p1);
        when(playerFactory.create("p2")).thenReturn(p2);
        Match match = new Match(p1.toString(), p2.toString(), playerFactory, setFactory);
        assertEquals("1-0", match.score());
    }

    @Test
    public void matchOver() throws Exception {
        Player p1 = new Player("p1");
        Player p2 = new Player("p2");
        Set set = mock(Set.class);
        // have the mock signal that the set was won
        when(set.pointWonBy(p2)).thenReturn(true);
        SetFactory setFactory = mock(SetFactory.class);
        PlayerFactory playerFactory = mock(PlayerFactory.class);
        when(playerFactory.create("p1")).thenReturn(p1);
        when(playerFactory.create("p2")).thenReturn(p2);
        when(setFactory.create(p1, p2)).thenReturn(set);
        Match match = new Match(p1.toString(), p2.toString(), playerFactory, setFactory);
        match.pointWonBy("p2");
        assertEquals("Game, Set, Match p2", match.score());
    }

    @Test(expected = IllegalStateException.class)
    public void cantContinueFinishedMatch() throws Exception {
        Player p1 = new Player("p1");
        Player p2 = new Player("p2");
        Set set = mock(Set.class);
        when(set.pointWonBy(p2)).thenReturn(true);
        SetFactory setFactory = mock(SetFactory.class);
        PlayerFactory playerFactory = mock(PlayerFactory.class);
        when(playerFactory.create("p1")).thenReturn(p1);
        when(playerFactory.create("p2")).thenReturn(p2);
        when(setFactory.create(p1, p2)).thenReturn(set);
        Match match = new Match(p1.toString(), p2.toString(), playerFactory, setFactory);
        match.pointWonBy("p2");
        assertEquals("Game, Set, Match p2", match.score());
        match.pointWonBy("p1");
    }

}