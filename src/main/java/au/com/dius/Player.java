package au.com.dius;

import java.util.Objects;

/**
 * @author  Willem Homan
 * @version 1.0
 * @since   2017-09-17
 * Player DTO
 */
public class Player {
    private String name;

    public Player(String name) {
        this.name = name;
    }
    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
    public String toString(){
        return name;
    }
}
