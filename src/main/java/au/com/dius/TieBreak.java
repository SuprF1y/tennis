package au.com.dius;

/**
 * Tiebreak specific game class
 * @author  Willem Homan
 * @version 1.0
 * @since   2017-09-17
 */

public class TieBreak extends AbstractGame {
    TieBreak(Player player1, Player player2, Score score, int minPointsToWin) {
        super(player1, player2, new Score(player1, player2), minPointsToWin);
    }

    /**
     * tie-breakers don't use the traditional tennis game scoring but use whole numbers instead
     */
    protected String getInProgressGameScore() {
        return score.get(player1) + "-" + score.get(player2);
    }

}
