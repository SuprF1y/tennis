package au.com.dius;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

public class GameFactoryTest {
    @Test
    public void noPoint() throws Exception {
        AbstractGame game = new GameFactory().create(new Player("p1"), new Player("p2"));
        assertThat(game, instanceOf(Game.class));
    }
    @Test
    public void midSet() throws Exception {
        AbstractGame game = new GameFactory().create(new Player("p1"), new Player("p2"), 3, 4);
        assertThat(game, instanceOf(Game.class));
    }
    @Test
    public void tieBreaker() throws Exception {
        AbstractGame game = new GameFactory().create(new Player("p1"), new Player("p2"), 6, 6);
        assertThat(game, instanceOf(TieBreak.class));
    }
}
