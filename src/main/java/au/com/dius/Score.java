package au.com.dius;

import java.util.HashMap;
import java.util.Map;

/**
 * @author  Willem Homan
 * @version 1.0
 * @since   2017-09-17
 * Keeps count of scores for both players
 */
class Score {
    private final Player player1;
    private final Player player2;
    private Map<Player, Integer> points = new HashMap<>();

    Score(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        points.put(player1, 0);
        points.put(player2, 0);
    }

    /**
     * increase the players score by one if they have won a point
     * @param player the player to award the point to
     */
    void increment(Player player) {
        points.put(player, points.get(player) + 1);
    }

    /**
     * get the points won by a player
     * @param player the player
     * @return the number of points won by the player
     */
    int get(Player player) {
        return points.get(player);
    }

    /**
     * reset both players score to zero
     */

    void reset() {
        points.put(player1, 0);
        points.put(player2, 0);
    }
}
