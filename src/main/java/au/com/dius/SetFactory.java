package au.com.dius;

/**
 * create a Set for the provided players
 * @author  Willem Homan
 * @version 1.0
 * @since   2017-09-17
 */

public class SetFactory {
    private final GameFactory gameFactory;

    SetFactory(GameFactory gameFactory){
        this.gameFactory = gameFactory;
    }
    public Set create(Player player1, Player player2){
        return new Set(player1, player2, new Score(player1, player2), gameFactory);
    }
}
