package au.com.dius;

/**
 * @author  Willem Homan
 * @version 1.0
 * @since   2017-09-17
 * Create player DTO's from their name
 */
public class PlayerFactory {
    public Player create(String player){
        return new Player(player);
    }
}
