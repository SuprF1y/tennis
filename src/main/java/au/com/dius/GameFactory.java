package au.com.dius;
/**
 * Creates normal and tie-Break games depending on the set score
 * @return
 */
public class GameFactory {
    private static final int MIN_POINTS_TO_WIN_NORMAL_GAME = 4;
    private static final int MIN_POINTS_TO_TIEBREAK = 7;

    /**
     * create a normal game for two players
     * @param player1 player 1
     * @param player2 player 2
     * @return a non tie-break game
     */
    public AbstractGame create(Player player1, Player player2) {
        return new Game(player1, player2, new Score(player1, player2), GameFactory.MIN_POINTS_TO_WIN_NORMAL_GAME);
    }

    /**
     * create a normal game or tie-break depending on the score
     * @param player1 player 1
     * @param player2 player 2
     * @param gamesWon1 the number of games won by player 1 in the set
     * @param gamesWon2 the number of games won by player 1 in the set
     * @return either a tie-break or normal game
     */
    public AbstractGame create(Player player1, Player player2, int gamesWon1, int gamesWon2) {
        // if it's 6-6 we've reached a tie-breaker
        if (gamesWon1 == 6 && gamesWon2 == 6) {
            return new TieBreak(player1, player2, new Score(player1, player2), GameFactory.MIN_POINTS_TO_TIEBREAK);
        }
        // otherwise it is a normal game
        return new Game(player1, player2, new Score(player1, player2), GameFactory.MIN_POINTS_TO_WIN_NORMAL_GAME);
    }

}
