package au.com.dius;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class IntegrationTest {
    @Test
    public void nonTieBreak() throws Exception {
        SetFactory setFactory = new SetFactory(new GameFactory());
        Match match = new Match("player 1", "player 2", new PlayerFactory(), setFactory);
        match.pointWonBy("player 1");
        match.pointWonBy("player 2");
        // this will return "0-0, 15-15"
        assertEquals("0-0, 15-15", match.score());

        match.pointWonBy("player 1");
        match.pointWonBy("player 1");
        // this will return "0-0, 40-15"
        assertEquals("0-0, 40-15", match.score());

        match.pointWonBy("player 2");
        match.pointWonBy("player 2");
        // this will return "0-0, Deuce"
        assertEquals("0-0, Deuce", match.score());

        match.pointWonBy("player 1");
        // this will return "0-0, Advantage player 1"
        assertEquals("0-0, Advantage player 1", match.score());

        match.pointWonBy("player 1");
        // this will return "1-0"
        assertEquals("1-0", match.score());

        winSet(match, "player 2");
        assertEquals("1-1", match.score());

        winSet(match, "player 2");
        assertEquals("1-2", match.score());

        winSet(match, "player 2");
        assertEquals("1-3", match.score());

        winSet(match, "player 1");
        assertEquals("2-3", match.score());

        winSet(match, "player 2");
        assertEquals("2-4", match.score());

        winSet(match, "player 1");
        assertEquals("3-4", match.score());


        match.pointWonBy("player 2");
        match.pointWonBy("player 2");
        assertEquals("3-4, 0-30", match.score());
        winSet(match, "player 1");
        assertEquals("4-4", match.score());

        winSet(match, "player 2");
        assertEquals("4-5", match.score());

        winSet(match, "player 1");
        assertEquals("5-5", match.score());

        winSet(match, "player 2");
        assertEquals("5-6", match.score());

        winSet(match, "player 2");
        assertEquals("Game, Set, Match player 2", match.score());

    }

    @Test
    public void tieBreak() throws Exception {
        SetFactory setFactory = new SetFactory(new GameFactory());
        Match match = new Match("player 1", "player 2", new PlayerFactory(), setFactory);
        for ( int i=0; i<6; i++){
            winSet(match, "player 1");
            winSet(match, "player 2");
        }
        assertEquals("6-6", match.score());
        match.pointWonBy("player 1");
        assertEquals("6-6, 1-0", match.score());
    }
    protected void winSet(Match match, String player) throws Exception {
        for( int i=0; i<4; i++){
            match.pointWonBy(player);
        }
    }
}
