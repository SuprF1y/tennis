package au.com.dius;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class GameTest {
    @Test
    public void noPoint() throws Exception {
        Player p1 = new Player("p1");
        Player p2 = new Player("p2");
        AbstractGame game = new GameFactory().create(p1, p2);
        assertEquals("", game.score());
    }
    @Test
    public void gamePoint() throws Exception {
        Player p1 = new Player("p1");
        Player p2 = new Player("p2");
        AbstractGame game = new GameFactory().create(p1, p2);
        for (int i = 0; i < 3; i++) {
            game.pointWonBy(p1);
        }
        assertEquals("40-0", game.score());
        assertTrue(game.pointWonBy(p1));
        assertEquals("", game.score());
    }
    @Test
    public void pointWonBy() throws Exception {
        Player p1 = new Player("p1");
        Player p2 = new Player("p2");
        AbstractGame game = new GameFactory().create(p1, p2);
        assertFalse(game.pointWonBy(p1));
        assertEquals("15-0", game.score());
        assertFalse(game.pointWonBy(p1));
        assertEquals("30-0", game.score());
        assertFalse(game.pointWonBy(p2));
        assertEquals("30-15", game.score());
        assertFalse(game.pointWonBy(p2));
        assertEquals("30-30", game.score());
        assertFalse(game.pointWonBy(p2));
        assertEquals("30-40", game.score());
        assertFalse(game.pointWonBy(p1));
        assertEquals("Deuce", game.score());
        assertFalse(game.pointWonBy(p1));
        assertEquals("Advantage p1", game.score());
        assertFalse(game.pointWonBy(p2));
        assertEquals("Deuce", game.score());
        assertFalse(game.pointWonBy(p2));
        assertEquals("Advantage p2", game.score());
        assertTrue(game.pointWonBy(p2));
        assertEquals("", game.score());
    }
}
