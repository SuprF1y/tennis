package au.com.dius;

import static java.lang.Math.abs;
/**
 * Scores a set of Tennis.
 * @author  Willem Homan
 * @version 1.0
 * @since   2017-09-17
 */
public class Set implements Scorable<Player> {
    private final Player player1;
    private final Player player2;
    private AbstractGame game;
    private final Score score;
    private final GameFactory gameFactory;

    Set(Player player1, Player player2, Score score, GameFactory gameFactory) {
        this.player1 = player1;
        this.player2 = player2;
        this.score = score;
        this.gameFactory = gameFactory;
        this.game = gameFactory.create(player1, player2);
    }

    /**
     * @inheritDoc
     */
    public boolean pointWonBy(Player player) {
        boolean gameWon = game.pointWonBy(player);
        // if that was game point
        if (gameWon) {
            // add 1 to the games won by the player
            score.increment(player);
            // create a new game
            game = gameFactory.create(player1, player2, score.get(player1), score.get(player2));
        }

        if (!this.isWon()) {
            return false;
        }
        // if the set is won
        // reset games to zero
        score.reset();
        return true;
    }

    /**
     * has the set been won
     * @return true if the set is won, false otherwise
     */
    private boolean isWon() {
        int gamesWon1 = score.get(player1);
        int gamesWon2 = score.get(player2);

        // one of the players must have won at least six games to have won the set
        if (gamesWon1 < 6 && gamesWon2 < 6) {
            return false;
        }
        // if one of the players has won more than 6 games and has a lead of 2 they win the set
        if (abs(gamesWon1 - gamesWon2) >= 2) {
            return true;
        }
        // if one of the players has won at least six games, with a lead less than two..
        // the only way they can have won is if they won the tiebreaker in which case they will have won 7 games
        // otherwise the set is still in progress
        return gamesWon1 >= 7 || gamesWon2 >= 7;
    }

    /**
     * @inheritDoc
     */
    public String score() {
        String gameScore = game.score();
        String setScore = score.get(player1) + "-" + score.get(player2);
        // if no points have been won in the set then don't return anything
        if (setScore.equals("0-0") && gameScore.equals("")) return "";

        // otherwise return the set score
        if (!gameScore.equals("")) {
            // and if there is a game in progress include the game score
            setScore += ", " + gameScore;
        }
        return setScore;
    }

}
