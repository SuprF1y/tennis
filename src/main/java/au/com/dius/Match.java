package au.com.dius;


import java.util.HashMap;
import java.util.Map;
/**
 * Scores a tennis match. Matches are only one set.
 * @author  Willem Homan
 * @version 1.0
 * @since   2017-09-17
 */
public class Match implements Scorable<String>{
    /** the current set */
    private Set set;
    /** the winner */
    private String winner;
    /**  a map of the player names to player instances */
    private Map<String, Player> players = new HashMap<>();

    /**
     * Constructs a new match
     * @param playerOneName player 1
     * @param playerTwoName player 2
     * @param playerFactory factory for creating player DAO's from their name
     * @param setFactory factory for creating a set
     */
    public Match(String playerOneName, String playerTwoName, PlayerFactory playerFactory, SetFactory setFactory) {
        Player player1 = playerFactory.create(playerOneName);
        Player player2 = playerFactory.create(playerTwoName);
        this.set = setFactory.create(player1, player2);
        players.put(playerOneName, player1);
        players.put(playerTwoName, player2);
    }

    /**
     * Award a point to a player
     * @param player the player to award the point to
     * @return true if the match is complete, false if the match is still in progress
     * @throws IllegalStateException if a game is over and another point is attempted to be awarded to a player
     */
    public boolean pointWonBy(String player) throws IllegalStateException {
        // guard against awarding points after the match is complete
        if (matchOver()){
            throw new IllegalStateException("Match is over. Can't award any more points to " + player);
        }
        // apply the award of the point to the set
        boolean setWon = this.set.pointWonBy(players.get(player));

        // if the set is still in progress return
        if (!setWon){
            return false;
        }

        // if the set has been won as a result of winning this point
        // declare a winner
        winner = player;
        return true;
    }

    /**
     * generates a text representation of the score
     * @return a text representation of the score
     */
    public String score(){
        if (matchOver()) {
            return "Game, Set, Match " + winner;
        }
        return set.score();
    }

    /**
     * has the match been completed?
     * @return has the match been completed?
     */
    private boolean matchOver() {
        return winner != null;
    }
}
