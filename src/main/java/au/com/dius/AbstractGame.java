package au.com.dius;
/**
 * Common ancestor of normal and tiebreak games
 * @author  Willem Homan
 * @version 1.0
 * @since   2017-09-17
 */
abstract public class AbstractGame implements Scorable<Player>{
    final Player player1;
    final Player player2;

    protected final Score score;

    /** the minumum points needed to win a game. differs for normal games vs tiebreakers */
    private int minPointsToWin;

    AbstractGame(Player player1, Player player2, Score score, int minPointsToWin) {
        this.score = score;
        this.minPointsToWin = minPointsToWin;
        this.player1 = player1;
        this.player2 = player2;
    }

    /**
     * generates a text representation of the score for a game which has at least 1 point scored against it
     * @return the score
     */
    protected abstract String getInProgressGameScore();

    /**
     * award a point to the player
     * @param player the player to award the point to
     * @return false if the game is still in progress, true if it is completed
     */
    public boolean pointWonBy(Player player) {
        // increment the players game score
        score.increment(player);

        // if the game is still in progress return
        if (!this.isWon(player)) {
            return false;
        }
        // if the game is won reset the game in preparation of the next game
        score.reset();
        return true;
    }

    /**
     * has the game been won by the player
     * @param player the player to award the point to
     * @return true if the player has won the game, false if they have not
     */
    private boolean isWon(Player player) {
        // one of the players must reach at least min points to win game
        if (score.get(player) < this.minPointsToWin) {
            return false;
        }
        // and be 2 points ahead
        return Math.abs(score.get(player1) - score.get(player2)) > 1;
    }

    /**
     * return a textual representation of the game score
     * @return the score
     */
    public String score() {
        // if  a game has not started then don't output anything for the score
        if (score.get(player1) == 0 && score.get(player2) == 0) return "";

        return this.getInProgressGameScore();
    }

    /**
     * return the player who is currently leading the game
     * @return the leading player
     */
    Player getLeadingPlayer() {
        return score.get(player1) > score.get(player2) ? player1 : player2;
    }
}
