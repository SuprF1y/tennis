package au.com.dius;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class SetTest {

    @Test
    public void setPoint() throws Exception {
        Player p1 = new Player("p1");
        Player p2 = new Player("p2");
        Game game = mock(Game.class);
        GameFactory gameFactory = mock(GameFactory.class);
        when(gameFactory.create(p1, p2)).thenReturn(game);
        when(game.pointWonBy(p1)).thenReturn(true);
        when(game.score()).thenReturn("40-30");
        Set set = new Set(p1, p2, new Score(p1, p2), gameFactory);
        assertEquals("0-0, 40-30", set.score());
        when(gameFactory.create(anyObject(), anyObject(), anyInt(), anyInt())).thenReturn(game);
        assertFalse(set.pointWonBy(p1));
        when(game.score()).thenReturn("");
        assertEquals("1-0", set.score());
    }
    @Test
    public void setPointSixFour() throws Exception {
        Player p1 = new Player("p1");
        Player p2 = new Player("p2");
        Game game = mock(Game.class);
        GameFactory gameFactory = mock(GameFactory.class);
        when(gameFactory.create(p1, p2)).thenReturn(game);
        when(game.pointWonBy(p1)).thenReturn(true);
        when(game.pointWonBy(p2)).thenReturn(true);
        Set set = new Set(p1, p2, new Score(p1, p2), gameFactory);
        when(game.score()).thenReturn("");
        when(gameFactory.create(anyObject(), anyObject(), anyInt(), anyInt())).thenReturn(game);
        for( int i=1; i<5; i++){
            assertFalse(set.pointWonBy(p1));
            assertFalse(set.pointWonBy(p2));
            assertEquals(i + "-" + i, set.score());
        }
        assertEquals("4-4", set.score());
        assertFalse(set.pointWonBy(p1));
        assertEquals("5-4", set.score());
        assertTrue(set.pointWonBy(p1));
        // 6-4
        assertEquals("", set.score());
    }
    @Test
    public void setPointFiveSeven() throws Exception {
        Player p1 = new Player("p1");
        Player p2 = new Player("p2");
        Game game = mock(Game.class);
        GameFactory gameFactory = mock(GameFactory.class);
        when(gameFactory.create(p1, p2)).thenReturn(game);
        when(game.pointWonBy(p1)).thenReturn(true);
        when(game.pointWonBy(p2)).thenReturn(true);
        Set set = new Set(p1, p2, new Score(p1, p2), gameFactory);
        when(game.score()).thenReturn("");
        when(gameFactory.create(anyObject(), anyObject(), anyInt(), anyInt())).thenReturn(game);
        for( int i=1; i<6; i++){
            assertFalse(set.pointWonBy(p1));
            assertFalse(set.pointWonBy(p2));
            assertEquals(i + "-" + i, set.score());
        }
        assertEquals("5-5", set.score());
        assertFalse(set.pointWonBy(p2));
        assertEquals("5-6", set.score());
        assertTrue(set.pointWonBy(p2));
        // 5-7
        assertEquals("", set.score());
    }

    @Test
    public void matchPoint() throws Exception {
        Player p1 = new Player("p1");
        Player p2 = new Player("p2");
        Game game = mock(Game.class);
        GameFactory gameFactory = mock(GameFactory.class);
        when(gameFactory.create(p1, p2)).thenReturn(game);
        when(game.pointWonBy(p1)).thenReturn(true);
        when(game.score()).thenReturn("40-30");
        Set set = new Set(p1, p2, new Score(p1, p2), gameFactory);
        assertEquals("0-0, 40-30", set.score());
        when(gameFactory.create(anyObject(), anyObject(), anyInt(), anyInt())).thenReturn(game);
        for (int i = 1; i < 6; i++) {
            assertFalse(set.pointWonBy(p1));
            assertEquals(i + "-0, 40-30", set.score());
        }
        assertTrue(set.pointWonBy(p1));
        when(game.score()).thenReturn("");
        assertEquals("", set.score());
    }

    @Test
    public void normalPoint() throws Exception {
        Player p1 = new Player("p1");
        Player p2 = new Player("p2");
        Game game = mock(Game.class);
        GameFactory gameFactory = mock(GameFactory.class);
        when(gameFactory.create(p1, p2)).thenReturn(game);
        when(game.pointWonBy(p1)).thenReturn(false);
        when(game.score()).thenReturn("30-15");

        Set set = new Set(p1, p2, new Score(p1, p2), gameFactory);
        assertEquals("0-0, 30-15", set.score());
    }

}