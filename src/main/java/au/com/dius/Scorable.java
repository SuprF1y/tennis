package au.com.dius;

/**
 * This interface is implemented by classes which are able to keep score on a point by point basis
 */
public interface Scorable<T> {
    /**
     * gets the score
     * @return a text representation of the score
     */
    String score();

    /**
     * awards a point to a player
     * @param player the player to award the point to
     * @return true of the scorable entity has been won by the awarding of this point, false otherwise
     * @throws IllegalStateException
     */
    boolean pointWonBy(T player);

}
