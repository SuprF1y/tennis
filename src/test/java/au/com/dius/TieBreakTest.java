package au.com.dius;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class TieBreakTest {
    @Test
    public void noPoint() throws Exception {
        Player p1 = new Player("p1");
        Player p2 = new Player("p2");
        AbstractGame game = new GameFactory().create(p1, p2, 6, 6);
        assertEquals("", game.score());
    }
    @Test
    public void gamePoint() throws Exception {
        Player p1 = new Player("p1");
        Player p2 = new Player("p2");
        AbstractGame game = new GameFactory().create(p1, p2, 6, 6);
        for (int i = 0; i < 6; i++) {
            game.pointWonBy(p1);
        }

        assertEquals("6-0", game.score());
        assertTrue(game.pointWonBy(p1));
        assertEquals("", game.score());
    }
    @Test
    public void mustWinByAtLeastTwo() throws Exception {
        Player p1 = new Player("p1");
        Player p2 = new Player("p2");
        AbstractGame game = new GameFactory().create(p1, p2, 6, 6);
        for (int i = 0; i < 6; i++) {
            game.pointWonBy(p1);
            game.pointWonBy(p2);
        }
        assertEquals("6-6", game.score());

        assertFalse(game.pointWonBy(p1));
        assertEquals("7-6", game.score());

        assertTrue(game.pointWonBy(p1));
        assertEquals("", game.score());
    }
    @Test
    public void pointWonBy() throws Exception {
        Player p1 = new Player("p1");
        Player p2 = new Player("p2");
        AbstractGame game = new GameFactory().create(p1, p2, 6 , 6 );
        assertFalse(game.pointWonBy(p1));
        assertEquals("1-0", game.score());
        assertFalse(game.pointWonBy(p1));
        assertEquals("2-0", game.score());
        assertFalse(game.pointWonBy(p2));
        assertEquals("2-1", game.score());
        assertFalse(game.pointWonBy(p2));
        assertEquals("2-2", game.score());
        assertFalse(game.pointWonBy(p2));
        assertEquals("2-3", game.score());
        assertFalse(game.pointWonBy(p1));
        assertEquals("3-3", game.score());
        assertFalse(game.pointWonBy(p1));
        assertEquals("4-3", game.score());
        assertFalse(game.pointWonBy(p2));
        assertEquals("4-4", game.score());
        assertFalse(game.pointWonBy(p2));
        assertEquals("4-5", game.score());
        assertFalse(game.pointWonBy(p1));
        assertEquals("5-5", game.score());
        assertFalse(game.pointWonBy(p1));
        assertEquals("6-5", game.score());
        assertTrue(game.pointWonBy(p1));
        assertEquals("", game.score());
    }
}