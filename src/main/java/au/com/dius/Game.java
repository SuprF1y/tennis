package au.com.dius;

/**
 * A non tie-break game of Tennis
 * @author  Willem Homan
 * @version 1.0
 * @since   2017-09-17
 */
public class Game extends AbstractGame {

    /**
     * @inheritDoc
     */
    Game(Player player1, Player player2, Score score, int minPointsToWin) {
        super(player1, player2, score, minPointsToWin);
    }

    /**
     * @inheritDoc
     * @return
     */
    protected String getInProgressGameScore() {
        // advantage game
        if (isAdvantageGame()) {
            return getAdvantageGameScore();
        }
        // in progress prior to deuce
        return getPreDeuceScore(player1) + "-" + getPreDeuceScore(player2);
    }

    /**
     * if both players have 3 or more points it's an advantage game
     */
    private boolean isAdvantageGame() {
        return score.get(player1) >= 3 && score.get(player2) >= 3;
    }

    /**
     * the score for a game which has reached Deuce
     * @return the score
     */
    private String getAdvantageGameScore() {
        // one of the players must reach at least 5 points
        if (score.get(player1) == (score.get(player2))) return "Deuce";
        return "Advantage " + getLeadingPlayer();
    }
    /**
     * the score for a game which has not reached Deuce
     * @return the score
     */
    private String getPreDeuceScore(Player player) {
        String scoreString;
        switch (score.get(player)) {
            case 0:
                scoreString = "0";
                break;
            case 1:
                scoreString = "15";
                break;
            case 2:
                scoreString = "30";
                break;
            case 3:
                scoreString = "40";
                break;
            default:
                scoreString = "Deuce";
                break;
        }
        return scoreString;
    }

}
